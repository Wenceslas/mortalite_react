import React from 'react'
import Flag from './flag'
import {ColumnChart} from 'react-chartkick';
window.Chart = require('chart.js');
const xTitle='Age';
const yTitle = '% Mortalité';

const MortalityListItem = ({mortality})=>{
    console.log(mortality.male)
    console.log(mortality.female)

    const formatedDataMale = formatMortalityData(mortality.male)
    const formatedDataFemale = formatMortalityData(mortality.female)
   
    console.log(formatedDataMale)
    return (
           
               <tr>
                    <td ><Flag country={mortality.country} className="flag_medium" /></td>
                    <td className="col-md-6"><ColumnChart xtitle={xTitle}  ytitle={yTitle} max={30}  data={formatedDataMale} /></td>
                    <td className="col-md-6"><ColumnChart xtitle={xTitle}  ytitle={yTitle} max={30}  data={formatedDataFemale}  /></td>
               </tr> 
                
          
    )
}

function formatMortalityData(mortality) {
    const filteredData = mortality.filter((data)=>{
        if(data.age>=101){
            return false
        }else{
            return data
        }
    })
    const array = filteredData.map((data)=>{
       
            return [Number((data.age).toFixed(0)),Number((data.mortality_percent).toFixed(0))]
  
    })
    console.log(array)
    return array;
}

export default MortalityListItem 