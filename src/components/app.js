import React, { Component } from 'react';
import SearchBar from '../containers/search_bar'
import MortalityList from '../containers/mortality_list'
const DEFAULT_COUNRTY= 'Benin'
export default class App extends Component {
  render() {
    return (
          <div>
            <h1 className="title" >Rechercher la statique de mortalité de tous les pays du monde</h1>
            <SearchBar defaultCountry={DEFAULT_COUNRTY} />
            <MortalityList defaultCountry={DEFAULT_COUNRTY}/>
          </div>
    );
  }
}
