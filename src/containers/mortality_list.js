import React,{Component} from 'react'
import MortalityListItem from '../components/mortality_list_item'
import {getMortality} from '../actions/index'
import {bindActionCretors} from "redux"
import {connect} from "react-redux"
class MortalityList extends Component{
   /* componentWillMount(){
        this.props.getMortality(this.props.defaultCountry)
    }*/
    renderMortalities(){
        const {mortalities} = this.props
        return mortalities.map((data)=>{
            return <MortalityListItem key={data.country} mortality={data} />
        })
    }
    render (){
            console.log(this.props.mortalities)
        return(
                <div>
                    <table className="table">
                    <thead>
                        <tr>
                            <th >Pays</th>
                            <th className="col-md-6">Hommes</th>
                            <th className="col-md-6 ">Femmes</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderMortalities()}
                    </tbody> 
                    </table>
                </div>
            )
    }
}
const mapStateToProps=(state)=>{
    return{
        mortalities:state.mortality
    }
} 
/*function mapDispatchToProps(dispatch){
     return bindActionCretors({getMortality},dispatch)
 }*/
export default connect(mapStateToProps)(MortalityList)
